package in.silentsudo.springbootmongodbdatagenerator;

import com.github.javafaker.Faker;
import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.Schema;
import org.apache.pulsar.shade.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PulsarConfiguration {

    @Bean
    public PulsarClient pulsarClient() throws PulsarClientException {
        return PulsarClient.builder().serviceUrl("pulsar://localhost:6650").build();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }


    @Bean
    public Producer<User> pulsarStringProducer(PulsarClient pulsarClient) throws PulsarClientException {
        return pulsarClient.newProducer(Schema.JSON(User.class))
//                .topic("persistent://manning/ashish/mongo-users-in")
                .topic("persistent://manning/ashish/mongo-users-out-cmd")
                .create();
    }

    @Bean
    public Faker faker() {
        return Faker.instance();
    }
}
