package in.silentsudo.springbootmongodbdatagenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMongodbDataGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMongodbDataGeneratorApplication.class, args);
    }

}
