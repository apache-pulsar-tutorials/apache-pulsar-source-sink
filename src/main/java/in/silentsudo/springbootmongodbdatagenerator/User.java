package in.silentsudo.springbootmongodbdatagenerator;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "users")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Getter
@Setter
public class User {

    @Id
    private String id;

    private String name;

    private String favouriteAnimal;

    @CreatedDate
    private LocalDateTime createdAt;

    @LastModifiedDate
    private LocalDateTime lastModifiedAt;

    @Version
    private Integer version;

    public User(String name, String favouriteAnimal) {
        this.name = name;
        this.favouriteAnimal = favouriteAnimal;
    }
}
