package in.silentsudo.springbootmongodbdatagenerator;

import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.shade.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Slf4j
@Configuration
@EnableScheduling
@RequiredArgsConstructor
public class PulsarGeneratorScheduler {
    private final UserRepository userRepository;
    private final Producer<User> producer;
    private final Faker faker;
    private final ObjectMapper objectMapper;

    @Scheduled(cron = "0/1 * * * * *")
    public void generateData() throws PulsarClientException {
        final User user = new User(faker.name().fullName(), faker.animal().name());
        try {
            userRepository.save(user);
            producer.send(user);
            final String userJson = objectMapper.writeValueAsString(user);
            System.out.println("Sending data: " + userJson);
        } catch (Exception e) {
            System.err.println("Error sending message to producer");
        }
    }

}
