## Imp Directories
- /home/ashish/installed_apps/apache-pulsar-3.0.0/download
- /home/ashish/installed_apps/apache-pulsar-3.0.0/packages-storage

## List Installed Sink

```shell
pulsar-admin sink list
```

## List Available Sink

```shell
pulsar-admin sink list
```

## Create Sink

```shell
pulsar-admin sink create --sink-type mongo --sink-config-file $PWD/apache-pulsar-mongo-sink.yaml
```

## Read from input topics
```shell
pulsar-client consume persistent://manning/ashish/mongo-users-in --subscription-name pulsar-mongo-user-cmd-consumer --subscription-type Exclusive -n 0
```

## Create Connectors
```shell
 pulsar-admin source create --source-config-file $PWD/apache-pulsar-mongo-source.yaml --archive $PWD/libs/pulsar-io-debezium-mongodb-3.0.0.nar
```

## List Sources
```shell
pulsar-admin sources list --tenant manning --namespace ashish
```

## List Sinks
```shell
pulsar-admin sinks list --tenant manning --namespace ashish
```

## Create Mongo Sink
```shell
pulsar-admin sink create --sink-config-file $PWD/apache-pulsar-mongo-sink.yaml --inputs persistent://manning/ashish/mongo-users-out-cmd --archive $PWD/libs/pulsar-io-mongo-3.0.0.nar
```

### Debugging logs

```log
1997  pulsar-admin sink available-sinks
 1998  clear
 1999  pulsar-client consume persistent://manning/ashish/mongo-user-in --subscription-name pulsar-func-out-cmd-consumer --subscription-type Exclusive -n 0
 2000  pulsar-client consume persistent://manning/ashish/mongo-user-in --subscription-name pulsar-mongo-user-cmd-consumer --subscription-type Exclusive -n 0
 2001  pulsar-admin sources list
 2002  pulsar-admin source
 2003  pulsar-admin source create--help
 2004  pulsar-admin source create --help
 2005  pulsar-admin sources list
 2006  pulsar-admin sources list --tenant manning
 2007  pulsar-admin sources list --tenant manning --namespave ashish
 2008  pulsar-admin sources list --tenant manning --namespace ashish
 2009  pulsar-admin sinks list --tenant manning --namespace ashish
 2010  pulsar-admin sources list --tenant manning --namespace ashish
 2011  pulsar-admin source status --name mongo-users-source
 2012  pulsar-admin source get --name mongo-users-source
 2013  pulsar-admin source get --name mongo-users-source --tenant manning
 2014  pulsar-admin sources list
 2015  pulsar-admin sources list --tenant manning
 2016  pulsar-admin sources list --tenant manning --namespace ashish
 2017  pulsar-admin source get --name mongo-users-source --tenant manning --namespace ashish
 2018  pulsar-admin source delete --name mongo-users-source --tenant manning --namespace ashish
 2019  pulsar-admin sources list --tenant manning --namespace ashish
 2020  pulsar-admin source get --name mongo-users-source --tenant manning --namespace ashish
 2021  pulsar-client consume persistent://manning/ashish/mongo-users-out-cmd --subscription-name pulsar-mongo-user-cmd-consumer --subscription-type Exclusive -n 0
 2022  pulsar-admin sink get --name mongo-users-sink --tenant manning --namespace ashish
 2023  pulsar-admin sink status --name mongo-users-sink --tenant manning --namespace ashish
 2024  pulsar-admin source delete --name mongo-users-source --tenant manning --namespace ashish
 2025  pulsar-client consume persistent://manning/ashish/mongo-users-out-cmd --subscription-name pulsar-mongo-user-cmd-consumer --subscription-type Exclusive -n 0
 2026  pulsar-admin sink get --name mongo-users-sink --tenant manning --namespace ashish
 2027  pulsar-admin sink status --name mongo-users-sink --tenant manning --namespace ashish
 2028  pulsar-admin sinks reload
 2029  pulsar-admin sink status --name mongo-users-sink --tenant manning --namespace ashish
 2030  pulsar-admin sinks reload
 2031  pulsar-admin sink status --name mongo-users-sink --tenant manning --namespace ashish
 2032  pulsar-admin sink --help
 2033  pulsar-admin sink srart --name mongo-users-sink --tenant manning --namespace ashish
 2034  pulsar-admin sink start --name mongo-users-sink --tenant manning --namespace ashish
 2035  pulsar-admin sink restart --name mongo-users-sink --tenant manning --namespace ashish
 2036  pulsar-admin sink status --name mongo-users-sink --tenant manning --namespace ashish
```